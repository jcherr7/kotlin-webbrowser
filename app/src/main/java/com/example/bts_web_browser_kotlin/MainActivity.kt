package com.example.bts_web_browser_kotlin

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var webview:WebView
    lateinit var searchButton:ImageButton
    lateinit var backButton:ImageButton
    lateinit var urlText:EditText

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun searchMe() {

        var url:String = ""
        url = urlText.getText().toString()

        if (("\\s".toRegex()).containsMatchIn(url) || !(url.contains(".")))
        {
            url = "https://www.google.com/search?q=$url"
        }

        if (url.contains(".") && (!(url.contains("https://")||(url.contains("http://")))))
        {
            url = "http://$url"
        }

        webview.loadUrl(url)
        //Toast.makeText(this, url, Toast.LENGTH_SHORT).show()
        hideKeyboard()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webview = findViewById(R.id.webview)
        urlText = findViewById(R.id.url)
        searchButton = findViewById(R.id.search_btn)
        backButton = findViewById(R.id.back_btn)


        webview.loadUrl("http://www.google.com")
        webview.settings.javaScriptEnabled = true // we need to enable javascript
        webview.canGoBack()
        webview.webViewClient = WebClient(this)

        backButton.setOnClickListener {
            webview.goBack()
        }

        urlText.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                searchMe()
                return@OnKeyListener true
            }
            false
        })

        searchButton.setOnClickListener {
            searchMe()
        }

    }



    class WebClient internal constructor(private val activity: Activity): WebViewClient(){
        override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
        ): Boolean {
            view?.loadUrl(request?.url.toString())
            return true
        }

    }
}

